import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class RestAPI {
    String uri = "https://restful-booker.herokuapp.com";
    String token;
    int bookingId;



    @Test(priority = 1)
    public void createToken() {
        RestAssured.baseURI = uri;

        JSONObject jObj = new JSONObject();

        jObj.put("username", "admin");
        jObj.put("password", "password123");


        Response res = given()
                .contentType("application/json")
                .body(jObj)
                .post("/auth");

        token = res.getBody()
                .jsonPath()
                .getString("token");

        System.out.println(token);
    }

    @Test(priority = 2)
    public void getBookingIds() {
        RestAssured.baseURI = uri;

        Response res = given()
                .get("/booking");

        System.out.println(res.getBody().jsonPath().getString("bookingid"));
    }


    @Test(priority = 3)
    public void createBooking() {
        RestAssured.baseURI = uri;

        JSONObject jObj = new JSONObject();

        JSONObject jObj1 = new JSONObject();

        jObj1.put("checkin", "2018-01-01");
        jObj1.put("checkout", "2019-01-01");

        jObj.put("firstname", "Jim");
        jObj.put("lastname", "Brown");
        jObj.put("totalprice", 111);
        jObj.put("depositpaid", true);
        jObj.put("bookingdates", jObj1);
        jObj.put("additionalneeds", "Breakfast");


        Response res = given()
                .contentType("application/json")
                .body(jObj)
                .post("/booking");


        bookingId = res.getBody().jsonPath().getInt("bookingid");

        System.out.println(bookingId);

    }

    @Test(priority = 4)
    public void getBooking() {
        RestAssured.baseURI = uri;

        Response res = given()
                .get("/booking/" + bookingId);

        System.out.println(res.getBody().jsonPath().getString("$"));
    }

    @Test(priority = 5)
    public void partialUpdateBooking() {
        RestAssured.baseURI = uri;

        JSONObject jObj = new JSONObject();
        jObj.put("depositpaid", false);
        jObj.put("additionalneeds", "berries");

        Response res = given()
                .header("Content-Type","application/json")
                .header ("Accept","application/json")
                .header("Cookie", "token=" + token)
                .body(jObj)
                .patch("/booking/" + bookingId);

        int statusCode = res.getStatusCode();
        Assert.assertEquals(statusCode, 200);

        System.out.println(given().get("/booking/" + bookingId).getBody().jsonPath().getString("$"));

    }

    @Test(priority = 6)
    public void updateBooking() {
        RestAssured.baseURI = uri;

        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();

        jObj1.put("checkin", "2018-01-04");
        jObj1.put("checkout", "2019-01-07");

        jObj.put("firstname", "Jake");
        jObj.put("lastname", "Green");
        jObj.put("totalprice", 112);
        jObj.put("depositpaid", true);
        jObj.put("bookingdates", jObj1);
        jObj.put("additionalneeds", "Dinner");

        Response res = given()
                .header("Content-Type","application/json")
                .header ("Accept","application/json")
                .header("Cookie", "token=" + token)
                .body(jObj)
                .put("/booking/" + bookingId);

        int statusCode = res.getStatusCode();
        Assert.assertEquals(statusCode, 200);

        System.out.println(given().get("/booking/" + bookingId).getBody().jsonPath().getString("$"));

    }


    @Test(priority = 7)
    public void deleteBooking() {
        RestAssured.baseURI = uri;

        Response res = given()
                .header("Content-Type", "application/json")
                .header("Cookie", "token=" + token)
                .delete("/booking/" + bookingId);

        int statusCode = res.getStatusCode();
        Assert.assertEquals(statusCode, 201);

    }


}

